import ForgeUI, {
    render, useProductContext, useState,
    Code, Form, ProjectSettingsPage, TextField, UserPicker,
    Table, Row, Cell,
} from '@forge/ui';
import api, { route } from "@forge/api";

const getUsers = async (accountId) => {
    const results = await api.asApp().requestJira(route`/rest/api/3/users/search`);
    return await results.json();
}

const getUsersAndGroups = async (context, query) => {
    const searchParams = new URLSearchParams({
        query: 'b',
    })
    const response = await api.asApp().requestJira(route`/rest/api/3/groupuserpicker?${searchParams}`);
    return await response.json();
}

const App = () => {
    const context = useProductContext();
    let settings = {
        assignees: [
            {
                name: 'Story and epic assignees',
                JQL: "issueType IN (Story, Epic)",
                assignable: []
            },
            {
                name: 'Bug assignees',
                JQL: "issueType IN (Bug)",
                assignable: []
            }
        ]
    };
    const [formState, setFormState] = useState(undefined);
    const [usersAndGroups] = useState(async () => await getUsersAndGroups(context, 'b'));
    const onSubmit = async (formData) => {
        setFormState(formData);
    };

    return (
        <ProjectSettingsPage>
            <Code text={JSON.stringify(context, null, 2)} language={"json"} showLineNumbers={false} />
            <Code text={JSON.stringify(usersAndGroups, null, 2)} language={"json"} showLineNumbers={false} />
            <Form onSubmit={onSubmit}>
                <Table>
                    {settings.assignees.map((assigneeFilter, index) => (
                        <Row>
                            <Cell>
                                <TextField name={`assignee[${index}][name]`} label='Assignee'
                                    defaultValue={assigneeFilter.name} />
                            </Cell>
                            <Cell>
                                <TextField name={`assignee[${index}][jql]`} label='Filter (JQL)'
                                    defaultValue={assigneeFilter.JQL} />
                            </Cell>
                            <Cell>
                                <UserPicker name={`assignee[${index}][userList]`} label='Assignable users'
                                    isMulti={true}
                                    defaultValue={assigneeFilter.assignable} />
                            </Cell>
                        </Row>
                    ))}
                </Table>
            </Form>
        </ProjectSettingsPage>
    );
};
export const run = render(
    <App />
);