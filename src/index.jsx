import api, {route} from "@forge/api";
import ForgeUI, {
    render, useProductContext, useState,
    Fragment, Text, IssuePanel
} from "@forge/ui";

const fetchCommentsForIssue = async (issueIdOrKey) => {
    const res = await api
        .asUser()
        .requestJira(route`/rest/api/3/issue/${issueIdOrKey}/comment`);

    const data = await res.json();
    return data.comments;
};

const App = () => {
    const context = useProductContext();
    const [comments] = useState(async () => await fetchCommentsForIssue(context.platformContext.issueKey));

    return (
        <Fragment>
            <Text>{comments.length} comments on this issue</Text>
        </Fragment>
    );
};

export const run = render(
    <IssuePanel>
        <App/>
    </IssuePanel>
);