import api, {route} from "@forge/api";

async function getLinkedIssues(issueId) {
    const searchParams = new URLSearchParams({
        jql: `issue in linkedIssues('${issueId}')`,
    })
    const requestUrl = route`/rest/api/3/search?${searchParams}`;
    let response = await api
        .asApp()
        .requestJira(requestUrl, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        });

    if (response.status !== 200) {
        console.log(response.status);
        throw `Unable to add comment to issueId ${issueId} Status: ${response.status}.`;
    }

    return response.json();
}


async function addComment(issueId, comment) {
    const requestUrl = route`/rest/api/3/issue/${issueId}/comment`;
    let response = await api.asApp()
        .requestJira(requestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(comment)
        });

    if (response.status !== 201) {
        console.log(response.status);
        throw `Unable to add comment to issueId ${issueId} Status: ${response.status}.`;
    }

    return response.json();
}

export async function commentedIssue(event, context) {
    const response = await getLinkedIssues(event.issue.id);
    // TODO: Fix events looping and generating too many comments
    // response.issues.forEach(
    //     issue => addComment(issue.id, event.comment)
    // );
}