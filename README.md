# ForgeDemo

Sandbox project for Forge implementing :

* projectSetting module: settings.jsx
    * Display a form with default values
    * TODO: Store values to use them in another module (idea: Limit assignees from JQL)
* trigger on issue:commented event: events.jsx
    * On issue commented, load linked issues and apply comment to all
    * TODO: Uncomment and fix event propagation loop (idea: detect event author forge / not forge ?)
* issuePanel module: index.jsx
    * Display the number of comments on the issue

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

- Modify your app by editing the `src/index.jsx` file.

- Build and deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
